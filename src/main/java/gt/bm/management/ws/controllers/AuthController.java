package gt.bm.management.ws.controllers;

import gt.bm.management.entities.user.dto.AccountDTO;
import gt.bm.management.entities.user.dto.OAuthUserInfoDTO;
import gt.bm.management.ws.exceptions.FieldsMissingException;
import gt.bm.management.ws.services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping(value = "/api/auth", produces = {MediaType.APPLICATION_JSON_VALUE})
public class AuthController {

    @Autowired
    private AuthService authService;

    @Transactional(rollbackFor = Exception.class)
    @PostMapping(value = "/login")
    public ResponseEntity<OAuthUserInfoDTO> doLogin(
            @RequestBody AccountDTO account
    ) throws Exception {
        if (account == null) {
            throw new FieldsMissingException();
        }

        return new ResponseEntity<>(authService.authenticate(account), HttpStatus.OK);
    }

}
