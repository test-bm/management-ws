package gt.bm.management.ws.exceptions;

public class FieldsMissingException extends Exception {

    public FieldsMissingException() {
        super();
    }

    public FieldsMissingException(String message) {
        super(message);
    }
}
