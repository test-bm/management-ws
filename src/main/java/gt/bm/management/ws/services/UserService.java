package gt.bm.management.ws.services;

import gt.bm.management.entities.user.dto.AccountDTO;
import gt.bm.management.entities.user.dto.AccountReportDTO;
import gt.bm.management.ws.exceptions.EntityNotFoundException;
import gt.bm.management.ws.exceptions.InternalErrorException;
import org.springframework.data.domain.Page;

import javax.servlet.http.HttpServletRequest;

public interface UserService {

    AccountReportDTO doGetUserData(HttpServletRequest request);

    AccountReportDTO doGetUserById(Integer id);

    Integer doCreateUser(AccountDTO accountDTO) throws Exception;

    Integer doUpdateUser(AccountDTO accountDTO) throws EntityNotFoundException;

    void doDeleteUser(Integer id) throws InternalErrorException;

    Page<AccountReportDTO> doGetAllUsers(Integer pageNumber, Integer pageSize, String search);

}
