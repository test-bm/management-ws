package gt.bm.management.ws.services;

import gt.bm.management.config.security.JwtTokenProvider;
import gt.bm.management.entities.user.Account;
import gt.bm.management.entities.user.Role;
import gt.bm.management.entities.user.builders.AccountBuilder;
import gt.bm.management.entities.user.dto.AccountDTO;
import gt.bm.management.entities.user.dto.AccountReportDTO;
import gt.bm.management.ws.exceptions.DuplicateEntityException;
import gt.bm.management.ws.exceptions.EntityNotFoundException;
import gt.bm.management.ws.exceptions.FieldsMissingException;
import gt.bm.management.ws.exceptions.InternalErrorException;
import gt.bm.management.ws.user.repo.AccountRepo;
import gt.bm.management.ws.user.repo.RoleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

import static gt.bm.management.ws.util.Md5Encrypt.getMd5;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private AccountRepo accountRepo;

    @Autowired
    private RoleRepo roleRepo;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Override
    public AccountReportDTO doGetUserData(HttpServletRequest request) {
        return accountRepo.findByEmail(jwtTokenProvider.getUsername(jwtTokenProvider.resolveToken(request)));
    }

    @Override
    public AccountReportDTO doGetUserById(Integer id) {
        return accountRepo.findByIdAndActiveTrue(id);
    }

    @Override
    public Integer doCreateUser(AccountDTO accountDTO) throws Exception {
        Role role = this.getRoleByName(accountDTO.getRole().getName());

        Account account = accountRepo.findByEmailAndActiveTrue(accountDTO.getEmail());

        if (account != null) {
            throw new DuplicateEntityException("Email already exists");
        }

        account = new AccountBuilder()
                .setEmail(accountDTO.getEmail())
                .setName(accountDTO.getName())
                .setPassword(getMd5(accountDTO.getPassword()))
                .setSub(getMd5(accountDTO.getEmail() + new Date().getTime()))
                .setRole(role)
                .create();

        if (account == null) {
            throw new FieldsMissingException();
        }

        return accountRepo.save(account).getId();
    }

    @Override
    public Integer doUpdateUser(AccountDTO accountDTO) throws EntityNotFoundException {
        Role role = this.getRoleByName(accountDTO.getRole().getName());

        Account account = accountRepo.findByEmailAndActiveTrue(accountDTO.getEmail());

        if (account == null) {
            throw new EntityNotFoundException("Account");
        }

        account.setName(accountDTO.getName());
        account.setPassword(getMd5(accountDTO.getPassword()));
        account.setRole(role);

        return accountRepo.save(account).getId();
    }

    @Override
    public void doDeleteUser(Integer id) throws InternalErrorException {
        Account account = accountRepo.findById(id).orElseThrow(() -> new InternalErrorException(HttpStatus.BAD_REQUEST));
        account.setActive(false);
        accountRepo.save(account);
    }

    @Override
    public Page<AccountReportDTO> doGetAllUsers(Integer pageNumber, Integer pageSize, String search) {
        Integer page = pageNumber;
        if (pageNumber > 0) {
            page = pageNumber - 1;
        }

        if ("".equals(search)) {
            return accountRepo.findAllByActiveTrue(PageRequest.of(page, pageSize));
        } else {
            return accountRepo.findAllByActiveTrueAndSearch(("%" + search + "%"), PageRequest.of(page, pageSize));
        }
    }

    private Role getRoleByName(String roleName) throws EntityNotFoundException {
        Role role = roleRepo.findByNameAndActiveTrue(roleName);

        if (role == null) {
            throw new EntityNotFoundException("Role");
        }

        return role;
    }
}
