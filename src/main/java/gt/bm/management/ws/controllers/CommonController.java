package gt.bm.management.ws.controllers;

import gt.bm.management.ws.services.AddressService;
import gt.bm.management.ws.services.CommonService;
import gt.bm.management.entities.common.dto.DocumentTypeDTO;
import gt.bm.management.entities.common.dto.StateDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/common", produces = {MediaType.APPLICATION_JSON_VALUE})
public class CommonController {

    @Autowired
    private CommonService commonService;

    @Autowired
    private AddressService addressService;

    @Transactional(readOnly = true)
    @GetMapping("/documentTypes")
    public ResponseEntity<List<DocumentTypeDTO>> doGetDocumentTypes(
            @RequestParam(value = "countryCode") String countryCode
    ) {
        return new ResponseEntity<>(commonService.getDocumentTypesByCountry(countryCode), HttpStatus.OK);
    }

    @Transactional(readOnly = true)
    @GetMapping("/states")
    public ResponseEntity<List<StateDTO>> doGetStates(
            @RequestParam(value = "countryCode") String countryCode
    ) {
        return new ResponseEntity<>(addressService.getStatesByCountry(countryCode), HttpStatus.OK);
    }

}
