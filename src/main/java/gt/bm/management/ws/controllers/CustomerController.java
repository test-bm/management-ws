package gt.bm.management.ws.controllers;

import gt.bm.management.ws.exceptions.EntityNotFoundException;
import gt.bm.management.ws.exceptions.FieldsMissingException;
import gt.bm.management.ws.services.CustomerService;
import gt.bm.management.entities.customer.dto.CustomerDTO;
import gt.bm.management.entities.customer.dto.CustomerReportDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
@RequestMapping(value = "/api/customer", produces = {MediaType.APPLICATION_JSON_VALUE})
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @Transactional(readOnly = true)
    @GetMapping(value = "/{id}")
    public ResponseEntity<CustomerReportDTO> doGetCustomer(@PathVariable Integer id) throws Exception {
        CustomerReportDTO customerReportDTO = customerService.doGetCustomerById(id);

        if (customerReportDTO == null) {
            throw new EntityNotFoundException("Customer");
        }

        return new ResponseEntity<>(customerReportDTO, HttpStatus.OK);
    }

    @Transactional(readOnly = true)
    @GetMapping(value = "/all")
    public ResponseEntity<Page<CustomerReportDTO>> doGetAllCustomers(
            @RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
            @RequestParam(value = "pageSize", defaultValue = "30") Integer pageSize,
            @RequestParam(value = "search", required = false, defaultValue = "") String search
    ) {
        return new ResponseEntity<>(customerService.doGetAllCustomers(pageNumber, pageSize, search), HttpStatus.OK);
    }

    @Transactional(rollbackFor = Exception.class)
    @PostMapping(value = {"", "/"})
    public ResponseEntity<Integer> doCreateCustomer(
            @RequestParam(value = "isIndividualCustomer", defaultValue = "true") Boolean isIndividualCustomer,
            @RequestBody CustomerDTO customerDTO
    ) throws Exception {
        if (customerDTO == null) {
            throw new FieldsMissingException("CustomerDTO");
        }

        return new ResponseEntity<>(customerService.doCreateCustomer(isIndividualCustomer, customerDTO), HttpStatus.CREATED);
    }

    @Transactional(rollbackFor = Exception.class)
    @PutMapping(value = {"", "/"})
    public ResponseEntity<Integer> doUpdateCustomer(
            @RequestBody CustomerDTO customerDTO
    ) throws Exception {
        if (customerDTO == null) {
            throw new FieldsMissingException("CustomerDTO");
        }

        Integer idUpdated = customerService.doUpdateCustomer(customerDTO);

        if (idUpdated == 0) {
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }

        return new ResponseEntity<>(idUpdated, HttpStatus.OK);
    }

    @Transactional(rollbackFor = Exception.class)
    @DeleteMapping(value = {"", "/"})
    public ResponseEntity doDeleteCustomer(
            @RequestParam(value = "id") Integer id
    ) throws Exception {
        customerService.doDeleteCustomer(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
