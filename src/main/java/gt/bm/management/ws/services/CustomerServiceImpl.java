package gt.bm.management.ws.services;

import gt.bm.management.ws.exceptions.FieldsMissingException;
import gt.bm.management.ws.exceptions.InternalErrorException;
import gt.bm.management.entities.common.Document;
import gt.bm.management.entities.customer.Customer;
import gt.bm.management.entities.customer.builders.CustomerBuilder;
import gt.bm.management.entities.customer.dto.CustomerDTO;
import gt.bm.management.entities.customer.dto.CustomerReportDTO;
import gt.bm.management.ws.customer.repo.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepo customerRepo;

    @Autowired
    private CommonService commonService;

    @Autowired
    private AddressService addressService;

    @Override
    public CustomerReportDTO doGetCustomerById(Integer id) {
        return customerRepo.findByIdAndActiveTrue(id);
    }

    @Override
    public Integer doCreateCustomer(Boolean isIndividualCustomer, CustomerDTO customerDTO) throws Exception {

        if (customerDTO.getDocument() == null || customerDTO.getDocument().getDocumentType() == null) {
            throw new FieldsMissingException("Missing document or document type");
        }

        Document document = commonService
                .createNewDocument(customerDTO.getDocument().getNumber(), customerDTO.getDocument().getDocumentType());

        Customer customer;
        if (isIndividualCustomer) {
            customer = new CustomerBuilder()
                    .setName(customerDTO.getName())
                    .setLastName(customerDTO.getLastName())
                    .setFullName(customerDTO.getFullName())
                    .setPhoneNumber(customerDTO.getPhoneNumber())
                    .setGender(customerDTO.getGender())
                    .setBirthdate(customerDTO.getBirthdate())
                    .setEmail(customerDTO.getEmail())
                    .setDocument(document)
                    .setAddress(addressService.createAddress(customerDTO.getAddress()))
                    .createIndividualCustomer();
        } else {
            customer = new CustomerBuilder()
                    .setFullName(customerDTO.getFullName())
                    .setLegalRepresentative(customerDTO.getLegalRepresentative())
                    .setPhoneNumber(customerDTO.getPhoneNumber())
                    .setEmail(customerDTO.getEmail())
                    .setDocument(document)
                    .setAddress(addressService.createAddress(customerDTO.getAddress()))
                    .createLegalCustomer();
        }

        return customerRepo.save(customer).getId();
    }

    @Override
    public Integer doUpdateCustomer(CustomerDTO customerDTO) throws Exception {
        Customer customer = findCustomerById(customerDTO.getId());

        Document document = commonService.findDocumentById(customerDTO.getDocument().getId());
        document.setNumber(customerDTO.getDocument().getNumber());

        customer.setAddress(addressService.updateAddress(customerDTO.getAddress()));
        customer.setBirthdate(customerDTO.getBirthdate());
        customer.setDocument(document);
        customer.setEmail(customerDTO.getEmail());
        customer.setFullName(customerDTO.getFullName());
        customer.setGender(customerDTO.getGender());
        customer.setLastName(customerDTO.getLastName());
        customer.setLegalRepresentative(customer.getLegalRepresentative());
        customer.setName(customerDTO.getName());
        customer.setPhoneNumber(customerDTO.getPhoneNumber());

        return customerRepo.save(customer).getId();
    }

    @Override
    public void doDeleteCustomer(Integer id) throws InternalErrorException {
        Customer customer = findCustomerById(id);

        customer.setActive(false);
        customer.getAddress().setActive(false);
        customer.getDocument().setActive(false);

        customerRepo.save(customer);
    }

    @Override
    public Page<CustomerReportDTO> doGetAllCustomers(Integer pageNumber, Integer pageSize, String search) {
        Integer page = pageNumber;
        if (pageNumber > 0) {
            page = pageNumber - 1;
        }

        if ("".equals(search)) {
            return customerRepo.findAllByActiveTrue(PageRequest.of(page, pageSize));
        } else {
            return customerRepo.findAllByActiveTrueAndSearch(("%" + search + "%"), PageRequest.of(page, pageSize));
        }
    }

    private Customer findCustomerById(Integer id) throws InternalErrorException {
        return customerRepo.findById(id).orElseThrow(() -> new InternalErrorException(HttpStatus.BAD_REQUEST));
    }
}
