package gt.bm.management.ws.exceptions;

public class ForbiddenException extends Exception {

    public ForbiddenException() {
        super();
    }

    public ForbiddenException(final String message) {
        super(message);
    }
}
