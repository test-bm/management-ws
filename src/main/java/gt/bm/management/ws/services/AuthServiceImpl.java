package gt.bm.management.ws.services;

import gt.bm.management.config.security.JwtTokenProvider;
import gt.bm.management.entities.user.Account;
import gt.bm.management.entities.user.dto.AccountDTO;
import gt.bm.management.entities.user.dto.OAuthUserInfoDTO;
import gt.bm.management.ws.exceptions.EntityNotFoundException;
import gt.bm.management.ws.exceptions.ForbiddenException;
import gt.bm.management.ws.user.repo.AccountRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static gt.bm.management.ws.util.Md5Encrypt.getMd5;

@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    private AccountRepo accountRepo;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Override
    public OAuthUserInfoDTO authenticate(AccountDTO account) throws Exception {

        Account user = accountRepo.findByEmailAndActiveTrue(account.getEmail());

        if (user == null) {
            throw new EntityNotFoundException("User not found");
        }

        if (user.getPassword().equals(getMd5(account.getPassword()))) {
            OAuthUserInfoDTO oAuthUserInfo = new OAuthUserInfoDTO();
            oAuthUserInfo.setEmail(account.getEmail());
            oAuthUserInfo.setName(user.getName());
            oAuthUserInfo.setRoleName(user.getRole().getName());
            oAuthUserInfo.setAccessToken(jwtTokenProvider.createToken(user.getEmail(), user.getRole().getName()));
            return oAuthUserInfo;
        } else {
            throw new ForbiddenException("Password incorrect");
        }
    }
}
