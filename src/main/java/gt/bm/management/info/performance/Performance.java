package gt.bm.management.info.performance;

import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/performance")
@FunctionalInterface
public interface Performance {

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    default String time() {
        long startTime = System.nanoTime();
        try {
            work();
        } catch (Exception e) {
            org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass());
            log.error("Error on Performance ", e);
        }

        long endTime = System.nanoTime();

        return "{ElapsedTime: " + Long.toString(endTime - startTime) + "}";
    }

    /**
     * Método provisto por la implementación, debería de ejecutar la ruta crítica
     * de la aplicación sin causar efectos secundarios.
     * <p>
     * Para una aplicación que implementa servicios web, se debería realizar una
     * consulta a la base de datos.
     */
    void work();

}
