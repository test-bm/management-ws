package gt.bm.management.config.security;

import gt.bm.management.entities.user.Account;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public final class UserDetailsBuilder {

    private UserDetailsBuilder() {
        // Private Constructor
    }

    public static UserDetails build(final Account account) {

        if (account == null) {
            throw new UsernameNotFoundException("User not found");
        }

        return new UserDetailsProxy(account);
    }

}
