package gt.bm.management.ws.services;

import gt.bm.management.ws.exceptions.EntityNotFoundException;
import gt.bm.management.entities.common.Document;
import gt.bm.management.entities.common.dto.DocumentTypeDTO;

import java.util.List;

public interface CommonService {

    Document createNewDocument(String number, DocumentTypeDTO documentType) throws Exception;

    Document findDocumentById(Integer documentId) throws EntityNotFoundException;

    List<DocumentTypeDTO> getDocumentTypesByCountry(String countryCode);

}
