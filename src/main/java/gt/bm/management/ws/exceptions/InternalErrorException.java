
package gt.bm.management.ws.exceptions;

import org.springframework.http.HttpStatus;

public class InternalErrorException extends Exception {

    private final HttpStatus status;

    public InternalErrorException() {
        super();
        this.status = HttpStatus.INTERNAL_SERVER_ERROR;
    }

    public InternalErrorException(final HttpStatus status) {
        super();
        this.status = status;
    }

    public InternalErrorException(final String message, final HttpStatus status) {
        super(message);
        this.status = status;
    }

    public InternalErrorException(final String message) {
        super(message);
        this.status = HttpStatus.INTERNAL_SERVER_ERROR;
    }

    public HttpStatus getStatus() {
        return status;
    }
}
