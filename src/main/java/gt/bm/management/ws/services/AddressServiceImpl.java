package gt.bm.management.ws.services;

import gt.bm.management.ws.exceptions.FieldsMissingException;
import gt.bm.management.ws.exceptions.InternalErrorException;
import gt.bm.management.entities.common.Address;
import gt.bm.management.entities.common.Country;
import gt.bm.management.entities.common.State;
import gt.bm.management.entities.common.builders.AddressBuilder;
import gt.bm.management.entities.common.builders.CountryBuilder;
import gt.bm.management.entities.common.builders.StateBuilder;
import gt.bm.management.entities.common.dto.AddressDTO;
import gt.bm.management.entities.common.dto.CountryDTO;
import gt.bm.management.entities.common.dto.StateDTO;
import gt.bm.management.ws.common.repo.AddressRepo;
import gt.bm.management.ws.common.repo.CountryRepo;
import gt.bm.management.ws.common.repo.StateRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    private CountryRepo countryRepo;

    @Autowired
    private StateRepo stateRepo;

    @Autowired
    private AddressRepo addressRepo;

    @Override
    public Address createAddress(AddressDTO addressDTO) throws FieldsMissingException {
        Address address = new AddressBuilder()
                .setAddressDescription(addressDTO.getAddressDescription())
                .setNumber(addressDTO.getNumber())
                .setZoneNumber(addressDTO.getZoneNumber())
                .setStreet(addressDTO.getStreet())
                .setResidential(addressDTO.getResidential())
                .setState(createState(addressDTO.getState()))
                .build();

        if (address == null) {
            throw new FieldsMissingException("Address");
        }

        return addressRepo.save(address);
    }

    @Override
    public Address updateAddress(AddressDTO addressDTO) throws InternalErrorException {
        Address address = addressRepo.findById(addressDTO.getId())
                .orElseThrow(() -> new InternalErrorException("Address not exist", HttpStatus.BAD_REQUEST));

        State state = stateRepo.findByCodeAndActiveTrue(addressDTO.getState().getCode());

        address.setAddressDescription(addressDTO.getAddressDescription());
        address.setState(state);
        address.setNumber(addressDTO.getNumber());
        address.setResidential(addressDTO.getResidential());
        address.setStreet(addressDTO.getStreet());
        address.setZoneNumber(addressDTO.getZoneNumber());

        return addressRepo.save(address);
    }

    @Override
    public List<StateDTO> getStatesByCountry(String countryCode) {
        return stateRepo.findByCountryAndActiveTrue(countryCode);
    }

    private State createState(StateDTO stateDTO) throws FieldsMissingException {
        State state = stateRepo.findByCodeAndActiveTrue(stateDTO.getCode());

        if (state == null) {
            state = new StateBuilder()
                    .setCode(stateDTO.getCode())
                    .setName(stateDTO.getName())
                    .setCountry(createCountry(stateDTO.getCountry()))
                    .build();

            if (state == null) {
                throw new FieldsMissingException("State");
            }
        }

        return state;
    }

    private Country createCountry(CountryDTO countryDTO) throws FieldsMissingException {
        Country country = countryRepo.findByCodeAndActiveTrue(countryDTO.getCode());

        if (country == null) {
            country = new CountryBuilder()
                    .setCode(countryDTO.getCode())
                    .setName(countryDTO.getName())
                    .build();

            if (country == null) {
                throw new FieldsMissingException("Country");
            }
        }

        return country;
    }
}
