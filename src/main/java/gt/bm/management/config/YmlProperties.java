package gt.bm.management.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(ignoreInvalidFields = true)
@Getter
public class YmlProperties {

    @Value("${gt.bm.management.origins}")
    private String allowOrigins;

}
