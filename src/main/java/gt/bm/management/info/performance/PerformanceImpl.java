package gt.bm.management.info.performance;

import gt.bm.management.ws.customer.repo.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PerformanceImpl implements Performance {

    @Autowired
    private CustomerRepo customerRepo;

    @Override
    public void work() {
        customerRepo.findById(1);
    }
}
