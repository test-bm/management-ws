package gt.bm.management.ws.controllers;

import gt.bm.management.entities.user.dto.AccountDTO;
import gt.bm.management.entities.user.dto.AccountReportDTO;
import gt.bm.management.ws.exceptions.EntityNotFoundException;
import gt.bm.management.ws.exceptions.FieldsMissingException;
import gt.bm.management.ws.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/api/user", produces = {MediaType.APPLICATION_JSON_VALUE})
public class UserController {

    @Autowired
    private UserService userService;

    @Transactional(readOnly = true)
    @GetMapping(value = {"", "/"})
    public ResponseEntity<AccountReportDTO> doGetUserData(HttpServletRequest request) {
        return new ResponseEntity<>(userService.doGetUserData(request), HttpStatus.OK);
    }

    @Transactional(readOnly = true)
    @GetMapping(value = "/{id}")
    public ResponseEntity<AccountReportDTO> doGetUser(@PathVariable Integer id) throws Exception {
        AccountReportDTO accountReportDTO = userService.doGetUserById(id);

        if (accountReportDTO == null) {
            throw new EntityNotFoundException("User");
        }

        return new ResponseEntity<>(accountReportDTO, HttpStatus.OK);
    }

    @Transactional(readOnly = true)
    @GetMapping(value = "/all")
    public ResponseEntity<Page<AccountReportDTO>> doGetAllUsers(
            @RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
            @RequestParam(value = "pageSize", defaultValue = "30") Integer pageSize,
            @RequestParam(value = "search", required = false, defaultValue = "") String search
    ) {
        return new ResponseEntity<>(userService.doGetAllUsers(pageNumber, pageSize, search), HttpStatus.OK);
    }

    @Transactional(rollbackFor = Exception.class)
    @PostMapping(value = {"", "/"})
    public ResponseEntity<Integer> doCreateUser(
            @RequestBody AccountDTO accountDTO
    ) throws Exception {
        if (accountDTO == null) {
            throw new FieldsMissingException("AccountDTO");
        }

        return new ResponseEntity<>(userService.doCreateUser(accountDTO), HttpStatus.CREATED);
    }

    @Transactional(rollbackFor = Exception.class)
    @PutMapping(value = {"", "/"})
    public ResponseEntity<Integer> doUpdateUser(
            @RequestBody AccountDTO accountDTO
    ) throws Exception {
        if (accountDTO == null) {
            throw new FieldsMissingException("AccountDTO");
        }

        Integer idUpdated = userService.doUpdateUser(accountDTO);

        if (idUpdated == 0) {
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }

        return new ResponseEntity<>(idUpdated, HttpStatus.OK);
    }

    @Transactional(rollbackFor = Exception.class)
    @DeleteMapping(value = {"", "/"})
    public ResponseEntity doDeleteUser(
            @RequestParam(value = "id") Integer id
    ) throws Exception {
        userService.doDeleteUser(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
