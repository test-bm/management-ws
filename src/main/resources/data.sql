 /**
  * Author: Steven Vargas
  * Execute querys
  */
insert into management.country
(active, version, created_at, created_by, code, name)
select 1, 0, now(), 'Anonimo', 'GT', 'Guatemala' where not exists(select id from management.country where code = 'GT');

insert into management.country
(active, version, created_at, created_by, code, name)
select 1, 0, now(), 'Anonimo', 'SV', 'El Salvador' where not exists(select id from management.country where code = 'SV');

insert into management.state
(active, version, created_at, created_by, code, name, country_id )
select 1, 0, now(), 'Anonimo', 'GTD-01', 'Alta Verapaz', (select id from management.country where code = 'GT')
where not exists(select id from management.state where code = 'GTD-01');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'GTD-02', 'Baja Verapaz', (select id from management.country where code = 'GT')
where not exists(select id from management.state where code = 'GTD-02');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'GTD-03', 'Chimaltenango', (select id from management.country where code = 'GT')
where not exists(select id from management.state where code = 'GTD-03');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'GTD-04', 'Chiquimula', (select id from management.country where code = 'GT')
where not exists(select id from management.state where code = 'GTD-04');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'GTD-05', 'El progreso', (select id from management.country where code = 'GT')
where not exists(select id from management.state where code = 'GTD-05');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'GTD-06', 'Escuintla', (select id from management.country where code = 'GT')
where not exists(select id from management.state where code = 'GTD-06');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'GTD-07', 'Guatemala', (select id from management.country where code = 'GT')
where not exists(select id from management.state where code = 'GTD-07');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'GTD-08', 'Huehuetenango', (select id from management.country where code = 'GT')
where not exists(select id from management.state where code = 'GTD-08');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'GTD-09', 'Izabal', (select id from management.country where code = 'GT')
where not exists(select id from management.state where code = 'GTD-09');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'GTD-10', 'Jalapa', (select id from management.country where code = 'GT')
where not exists(select id from management.state where code = 'GTD-10');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'GTD-11', 'Jutiapa', (select id from management.country where code = 'GT')
where not exists(select id from management.state where code = 'GTD-11');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'GTD-12', 'Petén', (select id from management.country where code = 'GT')
where not exists(select id from management.state where code = 'GTD-12');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'GTD-13', 'Quetzaltenango', (select id from management.country where code = 'GT')
where not exists(select id from management.state where code = 'GTD-13');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'GTD-14', 'Quiche', (select id from management.country where code = 'GT')
where not exists(select id from management.state where code = 'GTD-14');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'GTD-15', 'Retalhuleu', (select id from management.country where code = 'GT')
where not exists(select id from management.state where code = 'GTD-15');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'GTD-16', 'Sacatepequez', (select id from management.country where code = 'GT')
where not exists(select id from management.state where code = 'GTD-16');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'GTD-17', 'San Marcos', (select id from management.country where code = 'GT')
where not exists(select id from management.state where code = 'GTD-17');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'GTD-18', 'Santa Rosa', (select id from management.country where code = 'GT')
where not exists(select id from management.state where code = 'GTD-18');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'GTD-19', 'Solola', (select id from management.country where code = 'GT')
where not exists(select id from management.state where code = 'GTD-19');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'GTD-20', 'Suchitepequez', (select id from management.country where code = 'GT')
where not exists(select id from management.state where code = 'GTD-20');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'GTD-21', 'Totonicapan', (select id from management.country where code = 'GT')
where not exists(select id from management.state where code = 'GTD-21');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'GTD-22', 'Zacapa', (select id from management.country where code = 'GT')
where not exists(select id from management.state where code = 'GTD-22');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'SVD-01', 'Ahuachapan', (select id from management.country where code = 'SV')
where not exists(select id from management.state where code = 'SVD-01');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'SVD-02', 'Cabañas', (select id from management.country where code = 'SV')
where not exists(select id from management.state where code = 'SVD-02');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'SVD-03', 'Chalatenango', (select id from management.country where code = 'SV')
where not exists(select id from management.state where code = 'SVD-03');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'SVD-04', 'Cuscatlan', (select id from management.country where code = 'SV')
where not exists(select id from management.state where code = 'SVD-04');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'SVD-05', 'La Libertad', (select id from management.country where code = 'SV')
where not exists(select id from management.state where code = 'SVD-05');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'SVD-06', 'La Paz', (select id from management.country where code = 'SV')
where not exists(select id from management.state where code = 'SVD-06');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'SVD-07', 'La Unión', (select id from management.country where code = 'SV')
where not exists(select id from management.state where code = 'SVD-07');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'SVD-08', 'Morazan', (select id from management.country where code = 'SV')
where not exists(select id from management.state where code = 'SVD-08');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'SVD-09', 'San Miguel', (select id from management.country where code = 'SV')
where not exists(select id from management.state where code = 'SVD-09');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'SVD-10', 'San Salvador', (select id from management.country where code = 'SV')
where not exists(select id from management.state where code = 'SVD-10');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'SVD-11', 'San Vicente', (select id from management.country where code = 'SV')
where not exists(select id from management.state where code = 'SVD-11');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'SVD-12', 'Santa Ana', (select id from management.country where code = 'SV')
where not exists(select id from management.state where code = 'SVD-12');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'SVD-13', 'Sonsonate', (select id from management.country where code = 'SV')
where not exists(select id from management.state where code = 'SVD-13');

insert into management.state
(active, version, created_at, created_by, code, name, country_id)
select 1, 0, now(), 'Anonimo', 'SVD-14', 'Usulutan', (select id from management.country where code = 'SV')
where not exists(select id from management.state where code = 'SVD-14');

insert into management.document_type
(active, version, created_at, created_by, document_type_category, document_type_code, name, pattern, country_id)
select 1, 0, now(), 'Anonimo', 0, 0, 'DPI', NULL, (select id from management.country where code = 'GT')
where not exists(select id from management.document_type where document_type_category = 0 and document_type_code = 0 and country_id = (select id from management.country where code = 'GT'));

insert into management.document_type
(active, version, created_at, created_by, document_type_category, document_type_code, name, pattern, country_id)
select 1, 0, now(), 'Anonimo', 0, 1, 'Licencia de conducir', NULL, (select id from management.country where code = 'GT')
where not exists(select id from management.document_type where document_type_category = 0 and document_type_code = 1 and country_id = (select id from management.country where code = 'GT'));

insert into management.document_type
(active, version, created_at, created_by, document_type_category, document_type_code, name, pattern, country_id)
select 1, 0, now(), 'Anonimo', 0, 2, 'Pasaporte', NULL, (select id from management.country where code = 'GT')
where not exists(select id from management.document_type where document_type_category = 0 and document_type_code = 2 and country_id = (select id from management.country where code = 'GT'));

insert into management.document_type
(active, version, created_at, created_by, document_type_category, document_type_code, name, pattern, country_id)
select 1, 0, now(), 'Anonimo', 1, 0, 'NIT', NULL, (select id from management.country where code = 'GT')
where not exists(select id from management.document_type where document_type_category = 1 and document_type_code = 0 and country_id = (select id from management.country where code = 'GT'));

insert into management.document_type
(active, version, created_at, created_by, document_type_category, document_type_code, name, pattern, country_id)
select 1, 0, now(), 'Anonimo', 1, 3, 'Patente', NULL, (select id from management.country where code = 'GT')
where not exists(select id from management.document_type where document_type_category = 1 and document_type_code = 3 and country_id = (select id from management.country where code = 'GT'));

insert into management.document_type
(active, version, created_at, created_by, document_type_category, document_type_code, name, pattern, country_id)
select 1, 0, now(), 'Anonimo', 0, 0, 'DUI', NULL, (select id from management.country where code = 'SV')
where not exists(select id from management.document_type where document_type_category = 0 and document_type_code = 0 and country_id = (select id from management.country where code = 'SV'));

insert into management.document_type
(active, version, created_at, created_by, document_type_category, document_type_code, name, pattern, country_id)
select 1, 0, now(), 'Anonimo', 0, 1, 'Licencia de conducir', NULL, (select id from management.country where code = 'SV')
where not exists(select id from management.document_type where document_type_category = 0 and document_type_code = 1 and country_id = (select id from management.country where code = 'SV'));

insert into management.document_type
(active, version, created_at, created_by, document_type_category, document_type_code, name, pattern, country_id)
select 1, 0, now(), 'Anonimo', 0, 2, 'Pasaporte', NULL, (select id from management.country where code = 'SV')
where not exists(select id from management.document_type where document_type_category = 0 and document_type_code = 2 and country_id = (select id from management.country where code = 'SV'));

insert into management.document_type
(active, version, created_at, created_by, document_type_category, document_type_code, name, pattern, country_id)
select 1, 0, now(), 'Anonimo', 1, 0, 'NIT', NULL, (select id from management.country where code = 'SV')
where not exists(select id from management.document_type where document_type_category = 1 and document_type_code = 0 and country_id = (select id from management.country where code = 'SV'));

insert into management.document_type
(active, version, created_at, created_by, document_type_category, document_type_code, name, pattern, country_id)
select 1, 0, now(), 'Anonimo', 1, 3, 'Patente', NULL, (select id from management.country where code = 'SV')
where not exists(select id from management.document_type where document_type_category = 1 and document_type_code = 3 and country_id = (select id from management.country where code = 'SV'));

insert into management.rol
(active, version, created_at, created_by, name)
select 1, 0, now(), 'Anonimo', 'ROLE_ADMIN'
where not exists(select id from management.rol where name = 'ROLE_ADMIN');

insert into management.rol
(active, version, created_at, created_by, name)
select 1, 0, now(), 'Anonimo', 'ROLE_OPERATOR'
where not exists(select id from management.rol where name = 'ROLE_OPERATOR');

insert into management.rol
(active, version, created_at, created_by, name)
select 1, 0, now(), 'Anonimo', 'ROLE_USER'
where not exists(select id from management.rol where name = 'ROLE_USER');

insert into management.account
(active, version, created_at, created_by, email, name, password, sub, role_id)
select 1, 0, now(), 'Anonimo', 'admin@gmail.com', 'Administrador', '202cb962ac59075b964b07152d234b70', 'asdfghjkl', (select id from management.rol where name = 'ROLE_ADMIN')
where not exists(select id from management.account where sub = 'asdfghjkl');
