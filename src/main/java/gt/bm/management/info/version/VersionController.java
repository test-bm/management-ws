package gt.bm.management.info.version;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gt.bm.Version;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/version")
public class VersionController {

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, String> getVersion() {
        Map<String, String> version = new HashMap<>();
        version.put("Application", Version.getArtifactId());
        version.put("Organization", Version.getGroupId());
        version.put("Version", Version.getVersion());

        return version;
    }

}
