
package gt.bm.management.ws.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@ControllerAdvice
public class ExceptionsHandler {

    private Map<String, String> res = new ConcurrentHashMap<>();
    private static final String ERROR = "error";
    private static final String TEXT_MESSAGE = "message";
    private static final String ENTITY = "entity";

    @ResponseBody
    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    Map<String, String> entityNotFoundExceptionHandler(EntityNotFoundException ex) {
        this.res = new ConcurrentHashMap<>();
        this.res.put(ERROR, "Entidad no Encontrada");
        this.res.put(TEXT_MESSAGE, "El registro que se solicita no fue encontrado.");
        this.res.put(ENTITY, ex.getMessage());

        return this.res;
    }

    @ResponseBody
    @ExceptionHandler(FieldsMissingException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    Map<String, String> fieldsMissingExceptionHandler(FieldsMissingException exception) {
        this.res = new ConcurrentHashMap<>();
        this.res.put(ERROR, "Campos faltantes");
        this.res.put(TEXT_MESSAGE, "Esto sucede cuando faltan valores en campos requeridos.");
        this.res.put(ENTITY, exception.getMessage());

        return this.res;
    }

    @ResponseBody
    @ExceptionHandler(DuplicateEntityException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    Map<String, String> duplicateEntityExceptionHandler(DuplicateEntityException ex) {
        this.res = new ConcurrentHashMap<>();
        this.res.put(ERROR, "Entidad duplicada");
        this.res.put(TEXT_MESSAGE, "Esto sucede cuando se duplica la información.");
        this.res.put(ENTITY, ex.getMessage());

        return this.res;
    }

    @ResponseBody
    @ExceptionHandler(ForbiddenException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    Map<String, String> forbiddenExceptionHandler(ForbiddenException exception) {
        this.res = new ConcurrentHashMap<>();
        this.res.put(ERROR, "Recurso prohibido");
        this.res.put(TEXT_MESSAGE, "Esto sucede cuando intenta acceder a un recurso privado sin autenticación");

        return this.res;
    }

    @ResponseBody
    @ExceptionHandler(InternalErrorException.class)
    ResponseEntity<Map<String, String>> entityInternalServerErrorExceptionHandler(InternalErrorException ex) {
        this.res = new ConcurrentHashMap<>();
        this.res.put("Titulo", "Lo sentimos, estamos experimentando inconvenientes con nuestro servidor/aplicación.");
        this.res.put(ERROR, "Error Interno de la Aplicación");
        this.res.put(TEXT_MESSAGE, ex.getMessage());

        return new ResponseEntity<>(this.res, ex.getStatus());
    }

}
