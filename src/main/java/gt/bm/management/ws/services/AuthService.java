package gt.bm.management.ws.services;

import gt.bm.management.entities.user.dto.AccountDTO;
import gt.bm.management.entities.user.dto.OAuthUserInfoDTO;

public interface AuthService {

    OAuthUserInfoDTO authenticate(AccountDTO account) throws Exception;

}
