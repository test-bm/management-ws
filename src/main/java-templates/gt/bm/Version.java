package gt.bm;

public final class Version {

    private static final String VERSION_PROJECT = "${project.version}";
    private static final String GROUP_ID        = "${project.groupId}";
    private static final String ARTIFACT_ID     = "${project.artifactId}";
    private static final String BUILD_NUMBER    = "${buildNumber}";
    private static final String BRANCH          = "${scmBranch}";

    public Version() {
        // Empty
    }

    public static String getVersion() {
        return VERSION_PROJECT;
    }

    public static String getGroupId() {
        return GROUP_ID;
    }

    public static String getArtifactId() {
        return ARTIFACT_ID;
    }

    public static String getBuildNumber() {
        return BUILD_NUMBER;
    }

    public static String getBranch() {
        return BRANCH;
    }
}
