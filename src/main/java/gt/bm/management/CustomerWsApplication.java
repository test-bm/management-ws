package gt.bm.management;

import gt.bm.management.config.YmlProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource("classpath:application.yml")
@EnableConfigurationProperties(YmlProperties.class)
public class CustomerWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerWsApplication.class, args);
	}

}
