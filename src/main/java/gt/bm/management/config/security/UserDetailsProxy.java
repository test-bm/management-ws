package gt.bm.management.config.security;

import gt.bm.management.entities.user.Account;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashSet;

public class UserDetailsProxy implements UserDetails {

    private final Account account;

    public UserDetailsProxy(final Account account) {
        this.account = account;
    }

    public Account getAccount() {
        return this.account;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> collection = new HashSet<>();
        collection.add(getAccount().getRole());
        return collection;
    }

    @Override
    public String getPassword() {
        return "NA";
    }

    @Override
    public String getUsername() {
        return getAccount().getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
