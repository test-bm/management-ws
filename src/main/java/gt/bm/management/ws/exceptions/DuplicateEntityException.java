
package gt.bm.management.ws.exceptions;

public class DuplicateEntityException extends Exception {

    public DuplicateEntityException() {
        super();
    }

    public DuplicateEntityException(final String message) {
        super(message);
    }

}
