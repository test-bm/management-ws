package gt.bm.management.ws.services;

import gt.bm.management.ws.exceptions.FieldsMissingException;
import gt.bm.management.ws.exceptions.InternalErrorException;
import gt.bm.management.entities.common.Address;
import gt.bm.management.entities.common.dto.AddressDTO;
import gt.bm.management.entities.common.dto.StateDTO;

import java.util.List;

public interface AddressService {

    Address createAddress(AddressDTO addressDTO) throws FieldsMissingException;

    Address updateAddress(AddressDTO addressDTO) throws InternalErrorException;

    List<StateDTO> getStatesByCountry(String countryCode);

}
