package gt.bm.management.ws.services;

import gt.bm.management.ws.exceptions.DuplicateEntityException;
import gt.bm.management.ws.exceptions.EntityNotFoundException;
import gt.bm.management.ws.exceptions.FieldsMissingException;
import gt.bm.management.entities.common.Document;
import gt.bm.management.entities.common.DocumentType;
import gt.bm.management.entities.common.builders.DocumentBuilder;
import gt.bm.management.entities.common.dto.DocumentTypeDTO;
import gt.bm.management.ws.common.repo.DocumentRepo;
import gt.bm.management.ws.common.repo.DocumentTypeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommonServiceImpl implements CommonService {

    @Autowired
    private DocumentRepo documentRepo;

    @Autowired
    private DocumentTypeRepo documentTypeRepo;

    @Override
    public Document createNewDocument(String number, DocumentTypeDTO documentType) throws Exception {
        Document document = documentRepo.findByDocumentTypeIdAndNumber(documentType.getId(), number);

        if (document != null) {
            throw new DuplicateEntityException("A document with the same number already exists");
        }

        DocumentType documentT = documentTypeRepo.findById(documentType.getId())
                .orElseThrow(() -> new EntityNotFoundException("Document type not exist"));

        document = new DocumentBuilder()
                .setNumber(number)
                .setDocumentType(documentT)
                .build();

        if (document == null) {
            throw new FieldsMissingException();
        }

        return documentRepo.save(document);
    }

    @Override
    public Document findDocumentById(Integer documentId) throws EntityNotFoundException {
        return documentRepo.findById(documentId)
                .orElseThrow(() -> new EntityNotFoundException("Document not exist"));
    }

    @Override
    public List<DocumentTypeDTO> getDocumentTypesByCountry(String countryCode) {
        return documentTypeRepo.findByActiveTrueAndCountry(countryCode);
    }

}
