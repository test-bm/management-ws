package gt.bm.management.ws.services;

import gt.bm.management.ws.exceptions.InternalErrorException;
import gt.bm.management.entities.customer.dto.CustomerDTO;
import gt.bm.management.entities.customer.dto.CustomerReportDTO;
import org.springframework.data.domain.Page;

public interface CustomerService {

    CustomerReportDTO doGetCustomerById(Integer id);

    Integer doCreateCustomer(Boolean isIndividualCustomer, CustomerDTO customerDTO) throws Exception;

    Integer doUpdateCustomer(CustomerDTO customerDTO) throws Exception;

    void doDeleteCustomer(Integer id) throws InternalErrorException;

    Page<CustomerReportDTO> doGetAllCustomers(Integer pageNumber, Integer pageSize, String search);
}
