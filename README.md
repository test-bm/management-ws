# management-ws
Rest API for management

## Requirements
* Java 11
* Maven

## Project dependency
* ManagementEntities

### Run Tests

Open your terminal and navigate to the root folder of the project and run the command
```mvn clean install```

### Run API on localhost

Open your terminal and navigate to the root folder of the project and run the command
```mvn spring-boot:run```
